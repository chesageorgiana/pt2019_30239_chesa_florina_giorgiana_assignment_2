

//Clasa Casa implementeaza o casa din magazin

import java.util.Vector;

public class Casa extends Thread 
{
	private Vector <Client> cal=new Vector <Client>(); // vectorul de clienti ai magazinului
	private char status; // o casa poate fi deschisa ( 'd' ) sau inchisa ( 'i' )
	private int first;   // timpul necesar prelucrarii primul client din coada
	private int last;    // timpul necesar ultimului client de a fi procesat
	private int lungimeC; // lungimea cozii de clienti
		
	public Casa(char c)
	{
		status=c;
		first=0;
		last=0;
	}
	
	public int getFirst() // returneaza timpul necesar prelucrarii primul client din coada
	{
		return first;
	}
	
	public int getLast() // returneaza timpul necesar ultimului client de a fi procesat
	{
		return last;		
	}
	
	public void setFirst(int e) // seteaza timpul primului client de la casa
	{
		first=e;
	}
	
	public void setLast(int e) // seteaza timpul ultimului client de la casa
	{
		last=e;
	}
	
	public void setStatus(char c) // schimba statusul casei
	{
		status =c;
	}
	
	public char getStatus() // returneaza statusul casei
	{
		return status;
	}
	
	public Client getCalator(int i) //returneaza clientul de pe pozitia i
	{
		Client c = (Client) cal.elementAt(i);
		return c;
	}
	
	// adauga client la casa
	public synchronized void addC(Client c) throws InterruptedException 
	{		
		cal.addElement(c); //notifica celelalte fire de lucru ca sa nu mai adauge calatorul		
		notifyAll();	
	}
	
	public void setLungimeC(int l) // schimba lungimea cozii de clienti
	{
		lungimeC=l;
	}
	
	public int getLungimeC() // returneaza lungimea cozii de clienti
	{
		return lungimeC;
	}
	
	// sterge clientul din capul cozii
	public synchronized int stergeC() throws InterruptedException 
	{
		while(cal.size()==0)
			wait();			// daca coada e goala , nu se sterge nimic
		Client c = (Client) cal.elementAt(0);
		cal.removeElement(0); // stergerea propriu-zisa	
		notifyAll();
		return cal.elementAt(0).getStorn();		
	}	
	
	// tipareste continutul ( in ID ) a unei cozi 
	public synchronized void show() throws InterruptedException
	{
		for (int i=0;i<lungimeC; i++)
			System.out.println(cal.elementAt(i).getID());
	}
}



