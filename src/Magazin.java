
//Clasa Magazin, simuleaza comportamentul unui magazin pe durata unei zile

public class Magazin extends Thread 
{	
	private int nr_case;  				// numarul maxim al caselor deschise 
	private int nr_clienti; 			// numarul de clienti din ziua simulata
    private Casa casa[]=new Casa[10];	// vectorul de case ( maxim 10 disponibile intr-o zi )
	private Test t=new Test(); 			// instanta a clasei de test
	private Generate g=new Generate();	// instanta a clasei de generare
	private int w;						// memoreaza casa ce contine clientul care urmeaza sa fie sters
	private int mat[]=new int[1501];	// vector care memoreaza casa fiecarui client
	private int mat1[]=new int[1501];   // vector care memoreaza timpul pe care il petrece
										// clientul la coada
	
	public Magazin(int i,int j)	// constructorul clasei
	{							// initializeaza nr de clienti si nr de case (maxim) folosite
		nr_clienti=i;
		nr_case=j;
	}
	
	public void setNrCase(int n) // seteaza nr de case al magazinului
	{					
		nr_case=n;
	}
		
	public void setNrClienti(int n) // seteaza nr de clienti ai magazinului
	{
		nr_clienti=n;
	}
	
	private int min_index() // gaseste casa de lungime minima
	{
		int index=0;
		int min=casa[0].getLungimeC(); // pp ca e prima casa
		for (int i=1;i<nr_case;i++)
				{
				int lung=casa[i].getLungimeC();
				if ((lung < min) && (casa[i].getStatus()=='d')) 
					{							// daca exista o casa mai scurta si deschisa
					min=lung;
					index=i;							
					}
				}		
	return index; // returneaza rezultatul calculat
	}
	
	public void start()   // metoda apelata la pornirea executiei
	{
		for (int i=1;i<nr_case;i++) // toate casele sunt la inceput inchise si goale
							
							casa[i]=new Casa('i');
							
			casa[0]=new Casa('d'); // casa 0 ( prima casa ) e deschisa mereu si goala , la inceput		
	
	}
	
	public void run()
	{		
		try {
			int i=0;
			int s=0;
			int m=0;
			int j=0;
			int x=0;
			int min=-1;
			while (i<nr_clienti) 	// cat timp mai exista clienti de sosit
			{			 
			 i++;
			 min = min_index();		// calculeaza casa la care se va pune clientul
			 if (i<nr_clienti/5)
			 		 		 x=g.genCustomerLength('1');	// generare specifica diminetii		 
			 if ((i>nr_clienti/5)&&(i<3*nr_clienti/5))
				 			 x=g.genCustomerLength('2');	// generare specifica amiezii
			 else
				 x=g.genCustomerLength('3');				// generare specifica serii
			 
			 // se calculeaza daca nu se poate deschide o casa noua
			 // conditia e ca lungimea casei la care se introduce clientul e mai mare decat 3
			
			 if (casa[min].getLungimeC()>3)	
								{				 				
								int y=0;
								while (y<nr_case)
											{
											if (casa[y].getStatus()=='i')
													{													
													casa[y].setStatus('d');													
													min=y;
													y=nr_case;
													}
											y++;
											}
																		
								}	
			 // se creeaza clientul			
			 Client c=new Client (i);	
			 mat[i]=min;			
			 
			 if ((int)casa[min].getLungimeC()==0) // daca e primul client al casei		
				{								  // se seteaza timpii casei cu cei ai clientului
				casa[min].setFirst(x);
				casa[min].setLast(x);
				}
			 
			 	c.setStorn(x);		
				
				casa[min].addC(c);				  // se adauga clientul la casa						
									
				casa[min].setLungimeC(casa[min].getLungimeC()+1); // se modifica lungimea casei	
				casa[min].setLast(casa[min].getLast() + x); 	  // se seteaza timpul sau				
				mat1[i]=casa[min].getLast();
			
			 
			 // daca o casa diferita de casa 0 e goala,ea se inchide 	
			
			 if (((int)casa[min].getLungimeC()==0) && (min>0))
			 	casa[min].setStatus('i');
			 
			 t.update(min,(int)casa[min].getLungimeC()-1,i);	
			 
			 	 
			 if (i<nr_clienti-1)
				 			s=g.nextC(); // vine urmatorul client
			 	else
					s=1000000; 	// ultimul client
			 
			 m=minC();	 // se calculeaza timpul in care una din case se elibereaza
			 
			 if (s<m)
			 		{			 		
				 	updateA(s);	// daca vine urmatorul client mai repede
			 		sleep(s);	// se sare la momentul venirii sale
			 		}			// si se updateaza timpii tuturor caselor
			 else
			   {
				j++;						// semnifica faptul ca un client a parasit casa 	
				int x1=casa[w].stergeC();	// stergerea clientului respectiv din casa	
				casa[mat[j]].setFirst(x1);
				t.updateRem(mat[j],j,mat1[j]);
				casa[mat[j]].setLungimeC(casa[w].getLungimeC()-1);	// actualizarea lungimii casei
				if ((casa[w].getLungimeC()==0) && (w>0))
										casa[w].setStatus('i');	// verifica daca se poate inchide casa		
				updateA(m);				
				sleep(m);
			   }		  	
		  	 
			}	
			
			// toti clientii au sosit
			// se elibereaza toate casele care mai au clienti
			
			for (i=0;i<nr_case;i++)
				if (casa[i].getStatus()=='d')
				{
					int y=casa[i].getLungimeC();
					while (y>0)
					{
						j++;
					    casa[i].stergeC();
						t.updateRem(mat[j],j,mat1[j]);					
						casa[i].setLungimeC(casa[i].getLungimeC()-1);
						y--;
					}
				}
			t.end();							
			
		}
		catch (InterruptedException e)
		{
			System.out.println("catch");
			}
		
	}	
	
	// metoda care calculeaza care casa (si in cat timp) se va elibera prima
	public int minC()
	{
		int min=casa[0].getFirst();	
		for (int i=0;i<nr_case;i++)
			if ((min>casa[i].getFirst()) && (casa[i].getStatus()=='d')) 
				{
					min=casa[i].getFirst();		
					w=i;																			
				}	
		
		return min;
	}

	// metoda care actualizeaza timpii primului si ultimului client al casei
	// se apeleaza dupa ce un client a venit
	public void updateA(int x)
	{
		for (int i=0;i<nr_case;i++)
			if (casa[i].getStatus()=='d')
			{
				casa[i].setFirst(casa[i].getFirst()-x);							
				casa[i].setLast(casa[i].getLast()-x);
			}		
	}
		
}





