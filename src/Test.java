
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.event.*;
import java.awt.GridLayout;

// Clasa Test cuprinde metoda main care se ruleaza

public class Test 
{
	private static JTextField tf=new JTextField(5);		// TextFielduri care sunt
	private static JTextField tf2=new JTextField(5);	// folosite la input
	private static int cl;								// cl este nr de clienti care se introduce
	private static int ca;								// ca este nr maxim de case care se introduce
	private static JTextArea ta=new JTextArea(200,80);	// mesajele catre utilizator
	private static JPanel p2=new JPanel();
	private static JPanel p1=new JPanel();				// panelurile ferestrei
	private static JScrollPane p3 = new JScrollPane(ta); 
	
	private static JFrame fr=new JFrame("TP Tema 2 - cozi, Wrabeli Stefan, 30226"); // fereastra propriu-zisa
	private int total=0;								// timpul total petrecut de clienti la case
	
	// metoda care actualizeaza zona de mesaje , in cazul unei stergeri din coada
	public void updateRem(int c,int j,int t)
	{	
		
		int u=60+(t%100);
		ta.append("Pleca clientul " + j + " de la casa " + c + " dupa " + u + " secunde \n");
		total=total+u;						
	}
	
	// metoda care actualizeaza zona de mesaje , in cazul unei adaugari la coada
	public void update(int i,int j,int l) 
	{	
		ta.append("Soseste clientul " + l + " la casa " + i + "\n");			
	}
	
	// daca toti clientii au sosit , se furnizeaza rezultatele finale
	// se afiseaza timpul mediu petrecut de un client la coada
	public void end()
		{		
			ta.append("Timpul mediu este de " + ((int)total/cl) + " secunde\n");
		}
		
	// metoda main
	public static void main(String args[])
	{	
	 class SButtonListener implements ActionListener
     	{                
		 public void actionPerformed(ActionEvent event)
		 	{
			 	String s=tf.getText();
				cl=Integer.parseInt(s);		// se converteste continutul primului input	
				String s1=tf2.getText();
				ca=Integer.parseInt(s1);	// se converteste continutul celui de-al doilea input
				Magazin g=new Magazin(cl,ca); // se creeaza un magazin, cu "cl" clienti si "ca" case (maxim)
				g.start();					// se initializeaza magazinul
				g.run();					// incepe simularea				
		 	}            
     	}     
	 
	 class EButtonListener implements ActionListener // buton de iesire din interfata grafica
     	{	 
		 	public void actionPerformed(ActionEvent event)
		 		{
		 			System.exit(0);
		 		}
     	}
	
	JLabel lab1=new JLabel("Introduceti nr. de clienti: ");
	JLabel lab2=new JLabel("Introduceti nr. maxim de case: "); // etichete
	
	// se creeaza fereastra initiala
	p3.setBackground(Color.lightGray);
	tf.setSize(30,30);
	
	// se adauga etichetele si zonele de text la primul panel ( de sus )
	p1.add(lab1);
	p1.add(tf);
	p1.add(lab2);
	p1.add(tf2);
	
	p1.setBackground(Color.lightGray);
	p2.setBackground(Color.lightGray);
	
	JButton buton=new JButton("Start Simulation"); // butonul de incepere al simularii
	buton.setBackground(Color.white);
	ActionListener listener = new SButtonListener(); 
	buton.addActionListener(listener);	// adaugarea ascultatorului
	
	JButton buton1=new JButton("Exit"); // butonul de oprire a simularii si inchidere a ferestrei
	buton1.setBackground(Color.white);
	ActionListener listener2 = new EButtonListener(); 
	buton1.addActionListener(listener2);	// adaugarea ascultatorului
	
	p2.add(buton);
	p2.add(buton1);
		
	fr.add(p1,BorderLayout.NORTH);
	fr.add(p2,BorderLayout.SOUTH);
	fr.add(p3,BorderLayout.CENTER);
	fr.setSize(950,600);
	fr.setVisible(true);
	}
}
