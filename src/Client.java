
//Clasa Calator inglobeaza atributele si comportamentele unui client al magazinului

public class Client {
	
	private int id;  // id-ul sau ( al catelea client al acelei zile )
	private int storn; // timpul necesar procesarii cosului sau
		
	public Client(int id)	// Constructorul clasei, instantiaza id-ul
	{
		this.id=id;	
	}
	
	public int getID()	// metoda care returneaza id-ul clientului
	{
		return id;
	}

	public void setStorn(int s) // metoda care seteaza timpul de procesare al clientului
	{
		storn=s;
	}	
	
	public int getStorn() // metoda care returneaza timpul de procesare al clientului
	{
		return storn;
	}
	
}


