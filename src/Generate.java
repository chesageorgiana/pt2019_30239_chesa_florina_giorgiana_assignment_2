
//Clasa care genereaza aleator anumite valori , folosite ulterior in celelalte clase 

public class Generate 
{	
	// timpii de servire ( in secunde )
	private final int MORMIN = 30;
	private final int MORMAX = 120; // pentru dimineata
	private final int MIDMIN = 45;
	private final int MIDMAX = 180; // pentru amiaza
	private final int EVEMIN = 60;
	private final int EVEMAX = 240; // pentru seara
	
	public Generate()
	{		
	}
	
	public int nextC() // genereaza cand soseste urmatorul client la case
	{
		int x= (int)(Math.random()*1000)%500;
		return x;
	}
	
	public int genCustomerLength(char c) // genereaza timpul de stornare al clientului
	{									 // in functie de momentul zilei in care soseste
		int x=0;
		double d=Math.random();
		d = d * 1000;
		x = (int) d;
		if (c=='1')
					{					
					x = x %(MORMAX - MORMIN);	// pentru dimineata
					x = x + MORMIN;					
					return x+250;	
					}
		if (c=='2')
					{
					x = x %(MIDMAX - MIDMIN);  // pentru amiaza
					x = x + MIDMIN;					
					return x+250;
					}
		if (c=='3')
					{
					x = x %(EVEMAX - EVEMIN); // pentru seara
					x = x + EVEMIN;		
					return x+250;
					}
		return x;
	}	
	
}
